
// build.rs
extern crate chrono;
use chrono::prelude::*;

fn git_branch() -> Option<String>
{
    // $ git rev-parse --abbrev-ref HEAD
    let result = std::process::Command::new("git").args(&["rev-parse", "--abbrev-ref", "HEAD"]).output();
    result.ok().and_then(|output| {
        let v = String::from_utf8_lossy(&output.stdout).trim().to_string();

        if v.is_empty()
        {
            None
        }
        else
        {
            Some(v)
        }
    })
}

fn git_hash() -> Option<String>
{
    // $ git rev-parse HEAD
    let result = std::process::Command::new("git").args(&["rev-parse", "HEAD"]).output();
    result.ok().and_then(|output| {
        let v = String::from_utf8_lossy(&output.stdout).trim().to_string();
        if v.is_empty()
        {
            None
        }
        else
        {
            Some(v)
        }
    })
}

fn main() -> std::io::Result<()>
{
    let git_branch = git_branch().expect("Can't get git branch");
    let git_hash = git_hash().expect("Can't get git branch");
    let dt_local: DateTime<Local> = Local::now();

    println!("cargo:rustc-env=GIT_BUILD_HASH={}", git_hash);
    println!("cargo:rustc-env=GIT_BUILD_BRANCH={}", git_branch);
    println!("cargo:rustc-env=GIT_BUILD_DATETIME={}", dt_local.to_rfc2822());

    Ok(())
}
