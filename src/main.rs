
use semver::Version;
#[macro_use] extern crate clap;


/* log stuff */
#[macro_use] extern crate log;


use toml;
use chrono::prelude::*;
//use std::process::Command;
use std::process::{Command, Stdio};
use std::fmt::Display;
use std::io::prelude::*;
use serde_derive::Deserialize;
use mktemp::Temp;
use std::{fs, io, io::Write, fs::File, path::Path};

static DEFAULT_CHANGE_LOG : &'static str  = "debian/changelog";
static DEFAULT_RELEASE : &'static str  = "UNRELEASED";
static DEFAULT_EDITOR : &'static str  = "vi";

#[derive(Debug, Deserialize)]
struct Package {
    version: String
}

#[derive(Debug, Deserialize)]
struct CargoPkg
{
    package: Package
}

struct DebianChangelogEntry
{
    program_name:    String,
    program_version: Version,
    entry_deploy:    String,
    entry_urgency:   String,
    entry_text:      String,
    entry_author:    String,
    entry_date_str:  String,
}

impl DebianChangelogEntry
{
    fn new(name: &str, version: Version, deploy: &str, urgency: &str, text: &str, author: &str, date_str: &str) -> DebianChangelogEntry
    {
        DebianChangelogEntry {
            program_name:    name.into(),
            program_version: version,
            entry_deploy:    deploy.into(),
            entry_urgency:   urgency.into(),
            entry_text:      text.into(),
            entry_author:    author.into(),
            entry_date_str:  date_str.into(),
        }
    }
}

impl Display for DebianChangelogEntry
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result
    {
        write!(
            f,
            "{name} ({ver}) {deploy}; urgency={urgency}\n\n  {text}\n\n -- {author}  {date}\n\n",
            name = self.program_name,
            ver = self.program_version,
            deploy = self.entry_deploy,
            urgency = self.entry_urgency,
            text = self.entry_text,
            author = self.entry_author,
            date = self.entry_date_str
        )
    }
}

fn version_string() -> String
{
      format!(
          "{}\n    {}\n    {}\n    {}\n\n",
          format!(": {}", env!("CARGO_PKG_VERSION")),
          format!("Build Timestamp: {}", env!("GIT_BUILD_DATETIME")),
          format!("commit hash: {}", env!("GIT_BUILD_HASH")),
          format!("commit branch: {}", env!("GIT_BUILD_BRANCH")),
      )
}

/*
 * check $EDITOR, if not set use 'vi'
 */
fn edit_file<P: AsRef<Path>>(file: &P) -> bool {
    let editor_cmd = option_env!("EDITOR").unwrap_or(DEFAULT_EDITOR);

    let mut child = Command::new(editor_cmd)
            .args(&[file.as_ref().to_str().unwrap()] )
            .stderr(Stdio::null())
            .spawn()
            .expect("failed to execute process");

    let ecode = child.wait()
                 .expect("failed to wait on child");

    ecode.success()
}

fn git_name() -> String {
    let output = Command::new("git")
            .args(&["config", "user.name"])
            .output()
            .expect("failed to execute process");

    String::from_utf8_lossy(&output.stdout).into()
}


fn git_email() -> String {
    let output = Command::new("git")
            .args(&["config", "user.email"])
            .output()
            .expect("failed to execute process");

    String::from_utf8_lossy(&output.stdout).into()
}

fn repo_path() -> String  {
    // git rev-parse --show-toplevel
    let output = Command::new("git")
            .args(&["rev-parse", "--show-toplevel"]) 
            .output()
            .expect("failed to execute process");

    String::from_utf8_lossy(&output.stdout).into()
}

fn git_branch() -> Option<String>
{
    // $ git rev-parse --abbrev-ref HEAD
    let result = std::process::Command::new("git").args(&["rev-parse", "--abbrev-ref", "HEAD"]).output();
    result.ok().and_then(|output| {
        let v = String::from_utf8_lossy(&output.stdout).trim().to_string();

        if v.is_empty()
        {
            None
        }
        else
        {
            Some(v)
        }
    })
}
fn file_exists<S>(s: S) -> Result<(), String> where S: std::convert::AsRef<std::ffi::OsStr> + Display
{
    let p = std::path::Path::new(std::ffi::OsStr::new(&s));
    if p.exists() { Ok(()) } else { Err(format!("File {} doesn't exists", s)) }
}

fn main() -> Result<(), std::io::Error>
{

    env_logger::init();
    let development_branch_list = ["development", "develop"];
    let production_branch_list = ["production", "master", "main"];

    let git_email   = git_email();
    let git_name    = git_name();
    let git_branch_name = git_branch();

    let release =   
                    if git_branch_name.is_none() { DEFAULT_RELEASE }
                    else if development_branch_list.iter().any(|&x| git_branch_name.is_some() && x == git_branch_name.as_ref().unwrap()) { "development" }
                    else if production_branch_list.iter().any(|&x|  git_branch_name.is_some() && x == git_branch_name.as_ref().unwrap()) { "production" }
                    else  { DEFAULT_RELEASE };
    let matches = command!()
        //.arg(arg!(changelog: -f --file "Sets a custom changelog file").required(false).default_value(DEFAULT_CHANGE_LOG).validator(|x| file_exists(x)))
        .arg(arg!(changelog: -f --file "Sets a custom changelog file").required(false).default_value(DEFAULT_CHANGE_LOG))
        .arg(arg!(name: -n --name "Sets a custom git username (defailt from git-config)").required(false).default_value(&git_name.trim()))
        .arg(arg!(email: -e --email "Sets a custom git email (default from git-config)").required(false).default_value(&git_email.trim()))
        .arg(arg!(release: -r --release "Sets release").required(false).default_value(release))
        .arg(arg!(urgency: -u --urgency "Sets urgency").required(false).default_value("medium"))
        .version(crate::version_string().as_str())
        .author("Bradley Noyes <b@noyes.dev>")
        .about("Debian change-log program. Parses Cargo.toml and cmdline to populate debian changelog, then open editor")
        .get_matches();

        /*
    let matches = clap_app!(("dch-rs") =>
        (version:   v_string!().as_str())
        (author:    "Bradley Noyes <b@noyes.dev>")
        (about:     "Debian change-log program. Parses Cargo.toml and cmdline to pull out version info and populate debian change-log.")
  
        (@arg changelog:  -f --file <file> {file_exists} default_value(DEFAULT_CHANGE_LOG) "Sets a custom config file")
        (@arg name:       -n --name <name> default_value(&git_name.trim()) "Sets name (default from git-config)") 
        (@arg email:      -e --email <email> default_value(&git_email.trim()) "Sets email (default from git-config)") 
        //(@arg release:    -r --release <release> default_value(DEFAULT_RELEASE) "Sets release") 
        (@arg release:    -r --release [release]  default_value(release) "Sets release (default from git-config)") 
        (@arg urgency:    -u --urgency <urgency> default_value("medium") "Sets urgency") 
    )
    .get_matches();
        */



    let change_log_file = matches.value_of("changelog").unwrap();
    let _ = file_exists(change_log_file).map_err( |e|   { eprintln!("error: {}", e); std::process::exit(1) });
    let name            = matches.value_of("name").unwrap();
    let email           = matches.value_of("email").unwrap();
    let urgency         = matches.value_of("urgency").unwrap();
    let repo_path       = repo_path();
    let repo_path_str   = repo_path.trim();

    let proj_name = Path::new(repo_path_str).file_name().expect("Can't get git project path").to_str().unwrap();
    let now = Local::now().to_rfc2822();

    let mut input = String::new();
    let pkg_version : String  = File::open("Cargo.toml").and_then(|mut f| {
            f.read_to_string(&mut input)
        }).and_then(|_x| {
                let cargo_pkg :CargoPkg = toml::from_str(&*input)?; 
                let pkg_version = cargo_pkg.package.version;
                Ok(pkg_version)
            }
            ).or_else::<std::io::Error, _>(|_|
            {
                warn!("could not open Cargo.toml, using def version"); 
                Ok(String::from("0.0.0-0"))
            }
         ).unwrap();

    info!("proj: {:?}", proj_name);
    info!("changelog_file :{}", change_log_file);

    let t = DebianChangelogEntry::new(
            proj_name,
            Version::parse(&pkg_version).unwrap(), 
            release,
            urgency,
            "* <describe change here>",
            &format!("{} <{}>", name, email), 
            &now,
    );

    fs::copy(change_log_file, format!("{}.back", change_log_file )).expect("can't create backup of changelog");  

    let orig_path = Path::new(change_log_file);
    prepend_file(format!("{}", t).as_bytes(), &orig_path)

}

// from : https://stackoverflow.com/questions/43441166/prepend-line-to-beginning-of-file

fn prepend_file<P: AsRef<Path> >(data: &[u8], file_path: &P) -> io::Result<()> {

    // Create a temporary file 
    let tmp_path = Temp::new_file()?;
    // Stop the temp file being automatically deleted when the variable
    // is dropped, by releasing it.
    let p = tmp_path.release();
    // Open temp file for writing
    let mut tmp = File::create(&p)?;
    // Open source file for reading
    {
    let mut src = File::open(&file_path)?;
    // Write the data to prepend
    tmp.write_all(&data)?;
    // Copy the rest of the source file
    io::copy(&mut src, &mut tmp)?;
    }

    let before_mtime = std::fs::metadata(&p).unwrap().modified().unwrap();

    if edit_file(&p) {
        let after_mtime = std::fs::metadata(&p).unwrap().modified().unwrap();
        match after_mtime.duration_since(before_mtime) {
            Ok(t) => {
                if t.as_micros() == 0 {
                    // no change was made
                    eprintln!("No change, reverting");
                }
                else {
                    println!("Changelog updated");
                    fs::remove_file(&file_path)?;

                    // A simple rename does not work across filesystems so if /tmp is a different
                    // fs, fs::rename will error out
                    //fs::rename(&p, &file_path).or_else(|e| { eprintln!("Error: renaming {} ->  ({})", p.as_path().display(), e); Err(e) } )?;
                    fs::copy(&p, &file_path).or_else(|e| { eprintln!("Error: renaming {} ->  ({})", p.as_path().display(), e); Err(e) } )?;
                    fs::remove_file(&p)?;
                }
            }
            Err(_) => { 
                    eprintln!("Error No change, reverting");
            }
        }
    }
    else {
        eprintln!("Reverting changelog");
    }
    Ok(())
}

