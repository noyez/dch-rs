# dch-rs

## About

`dch-rs` pulls out version data from Cargo.toml and information from the
command line to populate a Debian Changelog. A more feature full program, `dch`
already exists from the debian project, however if you happen to be on a
non-linux platform, `dch` isn't available.

If a `--release <release>` argument is not specified on the command line,
`dch-rs` will detect the current git branch and populate the debian-release
field accordingly. For example, if current branch is `master` or `production`
then release will be set to `production`, if current branch is set to `develop`
or `development`, then release will be set to `development`


If email isn't specified then the result of `git config user.email` will be used.


## Install into cargo path

    git clone https://gitlab.com/noyez/dch-rs
    cd dch-rs
    cargo install --path .

## Usage

    # to just use default values from git
    dch-rs   # editor should open changelog.

    # to manually specify fields.
    dch-rs --file debian/changelog --name "First Last" --urgency medium --email first.last@youremail.com

